﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Examen
{
    class Program
    {
        public static void Main()
        {
            //7777777777777777777777777777777777777777777777777777777777777777777777777777777777777777
            int[,] _array = new int[4, 3] { { 1, 2, 3 }, { 4, 5, 6 }, { 0, 8, 9 }, { 10, 11, 12 } };
            var question7 = new Question7(_array);
            question7.PrintArray(_array);
            int[,] _newArray = question7.Response();
            Console.Write(Environment.NewLine + Environment.NewLine);
            question7.PrintArray(_newArray);
            //7777777777777777777777777777777777777777777777777777777777777777777777777777777777777777
            //6666666666666666666666666666666666666666666666666666666666666666666666666666666666666666
            var question6 = new Question6(10, 15);

            Console.Write(question6.Answer());
            //6666666666666666666666666666666666666666666666666666666666666666666666666666666666666666
            //5555555555555555555555555555555555555555555555555555555555555555555555555555555555555555
            var question5 = new Question5("aabcccccaaa");

            Console.Write(question5.Answer());
            //5555555555555555555555555555555555555555555555555555555555555555555555555555555555555555
            //4444444444444444444444444444444444444444444444444444444444444444444444444444444444444444
            var question4 = new Question4("hola", "aloh");
            Console.Write(question4.Answer());
            //4444444444444444444444444444444444444444444444444444444444444444444444444444444444444444
            //3333333333333333333333333333333333333333333333333333333333333333333333333333333333333333
            var question3 = new Question3("1558");

            Console.Write(question3.Answer());
            //3333333333333333333333333333333333333333333333333333333333333333333333333333333333333333
        }
    }

    public class Question7
    {

        ///Write a function such that if an element in an MxN matrix is 0, its entire row and column are set to 0 and then printed out. E.g.
        public Question7(int[,] _array)
        {
            ArrayToEvaluet = _array;
            RowLength = _array.GetLength(0);
            ColLength = _array.GetLength(1);
            LengthTotal = _array.Length;
            CloneArray();
        }

        private int LengthTotal { get; set; }
        private int RowLength { get; set; }
        private int ColLength { get; set; }
        private int[,] ArrayToEvaluet { get; set; }
        private int[,] NewArray { get; set; }

        private void CloneArray()
        {
            NewArray = new int[RowLength, ColLength];
            for (int i = 0; i < RowLength; i++)
            {
                for (int j = 0; j < ColLength; j++)
                {
                    NewArray[i, j] = ArrayToEvaluet[i, j];
                }
            }
        }

        public int[,] Response()
        {
            for (int i = 0; i < RowLength; i++)
            {
                for (int j = 0; j < ColLength; j++)
                {
                    if (ArrayToEvaluet[i, j] == 0)
                    {
                        ChangeRows(i);
                        ChangeCol(j);
                    }
                }
            }

            return NewArray;
        }
        private void ChangeRows(int current)
        {
            for (int j = 0; j < ColLength; j++)
            {
                NewArray[current, j] = 0;
            }
        }
        private void ChangeCol(int current)
        {
            for (int i = 0; i < RowLength; i++)
            {
                NewArray[i, current] = 0;
            }
        }
        public void PrintArray(int[,] _array)
        {
            int rowLength = _array.GetLength(0);
            int colLength = _array.GetLength(1);

            for (int i = 0; i < rowLength; i++)
            {
                for (int j = 0; j < colLength; j++)
                {
                    Console.Write(string.Format("{0} ", _array[i, j]));
                }
                Console.Write(Environment.NewLine + Environment.NewLine);
            }
        }
    }

    public class Question6
    {

        ///Swap two integers without using a temporary variable.
        public Question6(int firstInt, int secondInt)
        {
            FirstInt = firstInt;
            SecondInt = secondInt;
        }

        private int FirstInt { set; get; }
        private int SecondInt { set; get; }

        public string Answer()
        {
            FirstInt = FirstInt * SecondInt;
            SecondInt = FirstInt / SecondInt;
            FirstInt = FirstInt / SecondInt;
            return "FirstInt: " + FirstInt + ", SecondInt: " + SecondInt;
        }
    }

    public class Question5
    {

        ///Write a function to perform basic string compression using the counts of repeated characters; e.g. "aabcccccaaa" would become "a2b1c5a3". If the compressed string would not become smaller than the original string, just print the original string.
        public Question5(string stringToEvaluate)
        {
            StringToEvaluate = stringToEvaluate;
            LetterUsed = stringToEvaluate.ToCharArray().Select(x => x.ToString()).ToArray();
            Compressed = new StringBuilder();
        }

        private string StringToEvaluate { set; get; }
        private string[] LetterUsed { set; get; }
        private StringBuilder Compressed { set; get; }

        public string Answer()
        {
            for (int i = 0; i < LetterUsed.Length; i++)
            {
                int count = 0;
                for (int x = i + 1; x < LetterUsed.Length; x++)
                {
                    if (LetterUsed[i] == LetterUsed[x])
                    {
                        count++;
                    }
                    else
                    {
                        break;
                    }
                }
                Compressed.Append(LetterUsed[i] + (count + 1));
                if (count > 0)
                {
                    i += count;

                }
            }
            return Compressed.ToString();
        }
    }

    public class Question4
    {

        ///Write a function that, given two strings, test whether one is an anagram of the other. 		\

        public Question4(string first, string second)
        {
            First = first.ToCharArray().Select(c => c.ToString()).ToArray();
            Second = second.ToCharArray().Select(c => c.ToString()).ToArray();
            LengthFirst = First.Length;
            LengthSecond = Second.Length;
        }

        private string[] First { set; get; }
        private string[] Second { set; get; }
        private int LengthFirst { set; get; }
        private int LengthSecond { set; get; }

        public string Answer()
        {

            return TheyAreAnagramaEachOther()
                ? "The two strings are anagram of each other"
                : "The two strings are not anagram of each other";
        }
        private bool TheyAreAnagramaEachOther()
        {
            if (LengthFirst != LengthSecond)
                return false;

            for (int i = LengthFirst - 1; i >= 0; i--)
            {
                if (First[i] != Second[(LengthFirst - 1) - i])
                    return false;
            }

            return true;
        }
    }

    public class Question3
    {

        ///Write a function that converts a given integer into a Roman Numeral - the method needs to receive an integer and return a string (The integer can be between 1 and 3999). 
        public Question3(string number)
        {
            int _Number = 0;
            CanBeBecome = Int32.TryParse(number, out _Number);
            if (CanBeBecome && _Number <= 3999 && _Number >= 1)

                Number = _Number;
        }

        private int Number { set; get; }
        private bool CanBeBecome { set; get; }
        private StringBuilder RomanNumber { set; get; }

        private static Dictionary<string, int> RomanNumbers = new Dictionary<string, int>(){
            {"M",1000},
            {"CM",900},
            {"D",500},
            {"CD",400},
            {"C",100},
            {"XC",90},
            {"L",50},
            {"XL",40},
            {"X",10},
            {"IX",9},
            {"V",5},
            {"IV",4},
            {"I",1}}
        ;

        private string Divide(ref int current)
        {
            StringBuilder result = new StringBuilder();
            foreach (var r in RomanNumbers)
            {
                if (current == 0)
                    return result.ToString();
                if (current >= r.Value)
                {

                    result.Append(Convert(r.Key, (int)current / r.Value));

                    if (current % r.Value == 0)
                    {
                        current = 0;
                        return result.ToString();
                    }
                    else
                    {
                        current = current % r.Value;
                        result.Append(Divide(ref current));

                    }

                }
            }
            return result.ToString();
        }

        private string Convert(string letter, int times)
        {

            StringBuilder newString = new StringBuilder();
            for (int i = 0; i < times; i++)
            {

                newString.Append(letter);
            }
            return newString.ToString();
        }

        public string Answer()
        {
            if (!CanBeBecome)
                return "Invalid value";
            int _number = Number;

            string rr = Divide(ref _number);



            return rr;
        }
    }
}
